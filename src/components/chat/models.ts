export enum MessageType {
    MESSAGE_TYPE_ALL = 0,
    MESSAGE_TYPE_INBOX = 1,
    MESSAGE_TYPE_SENT = 2,
    MESSAGE_TYPE_DRAFT = 3,
    MESSAGE_TYPE_OUTBOX = 4,
    MESSAGE_TYPE_FAILED = 5, // for failed outgoing messages
    MESSAGE_TYPE_QUEUED = 6, //
}

export interface IChatMessage {
    type: MessageType;
    body: string;
    date: Date;
}
