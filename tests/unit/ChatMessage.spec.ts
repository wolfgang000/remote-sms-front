import { shallowMount } from '@vue/test-utils';
import ChatMessage from '@/components/chat/ChatMessage.vue';

describe('ChatMessage.vue', () => {
  it('formats dates', () => {
    // 24/11/2018 10:33
    const msg = {
        date: new Date(2018, 11, 24, 10, 33, 30, 0)
    };
    const wrapper = shallowMount(ChatMessage, {
      propsData: { msg },
    });
    expect((wrapper.vm as any).formatDate()).toMatch('Monday, December 24, 2018 | 10:33 AM');
  });
});
